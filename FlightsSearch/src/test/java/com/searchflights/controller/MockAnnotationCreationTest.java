package com.searchflights.controller;

import com.searchflights.config.ProvidersFilesConfig;
import com.searchflights.config.SwaggerConfig;
import com.searchflights.dto.FlightTimingDTO;
import com.searchflights.service.impl.SearchFlightsImpl;
import com.searchflights.utils.DateUtil;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author akash
 *
 *         Test the MockAnnotation
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WebAppConfiguration
public class MockAnnotationCreationTest {

    @Mock
    private SearchFlightsImpl searchFlights;
    @Mock
    private ProvidersFilesConfig providersFilesConfig;
    @Mock
    private SwaggerConfig swaggerConfig;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).dispatchOptions(true).build();
    }

    @Test
    public void testMockCreation(){
        assertNotNull(searchFlights);
        assertNotNull(providersFilesConfig);
        assertNotNull(swaggerConfig);
    }

    @Test
    public void testSearchFlights() throws IOException {
        List<FlightTimingDTO>  testList =   getMockListData();
        when(searchFlights.getSearchedFlights("LAS","LAX")).thenReturn(testList);
        assertEquals(testList,searchFlights.getSearchedFlights("LAS","LAX"));
        verify(searchFlights).getSearchedFlights("LAS", "LAX");
    }

    @Test
    public void testSearchFlightsNotFound() throws Exception {
        List<FlightTimingDTO>  testList  = notFoundData();
        when(searchFlights.getSearchedFlights("YYC", "YYZ")).thenReturn(testList);
        assertEquals(testList, searchFlights.getSearchedFlights("YYC", "YYZ"));

    }

    @Test
    public void testSearchFlightsHTTPNotFound() throws Exception {
        List<FlightTimingDTO>  testList = notFoundData();
        when(searchFlights.getSearchedFlights("YYC", "YYZ")).thenReturn(testList);
        mockMvc.perform(MockMvcRequestBuilders.get("/searchFlights/YYC/YYZ")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }

    private List<FlightTimingDTO> getMockListData(){

        List<FlightTimingDTO> list  = new ArrayList<>();
        FlightTimingDTO flightTimingDTO = new FlightTimingDTO();
        flightTimingDTO.setActualDepartureTime("6/15/2014 9:54:00");
        flightTimingDTO.setActualDestinationTime("6/15/2014 11:05:00");
        flightTimingDTO.setActualPrice(286.00F);
        flightTimingDTO.setDePartureTime(DateUtil.convertDate("6/15/2014 11:05:00"));
        flightTimingDTO.setDestination("LAX");
        flightTimingDTO.setDestinationTime(DateUtil.convertDate("6/15/2014 9:54:00"));
        flightTimingDTO.setoRigin("LAS");
        flightTimingDTO.setPrice("$286.00");

        list.add(flightTimingDTO);
        return list;
    }

    private List<FlightTimingDTO> notFoundData (){
        return new ArrayList<FlightTimingDTO>();
    }
}