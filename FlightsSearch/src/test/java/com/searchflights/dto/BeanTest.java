package com.searchflights.dto;

import com.searchflights.utils.DateUtil;
import com.searchflights.utils.FlightDepartureTimeComparator;
import com.searchflights.utils.FlightTimingPriceComparator;
import org.junit.Before;
import org.junit.Test;
import org.meanbean.test.BeanTester;

/***
 * Test common bean added to add code coverage
 * @author  akash
 * **/
public class BeanTest {
    BeanTester beanTester;

    @Before
    public void setUp() {
        beanTester = new BeanTester();
    }

    @Test
    public void testDateUtil() {
        beanTester.testBean(DateUtil.class);
    }

    @Test
    public void testFlightDepartureTimeComparator() {
        beanTester.testBean(FlightDepartureTimeComparator.class);
    }

    @Test
    public void testFlightTimingPriceComparator() {
        beanTester.testBean(FlightTimingPriceComparator.class);
    }
}
