package com.searchflights.dto;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author akash
 *
 *         Test the MockCreation Object.s
 */
public class MockCreationTest {
    private FlightTimingDTO flightTimingDTO;

    @Before
    public void setupMock() {
        flightTimingDTO = mock(FlightTimingDTO.class);
    }

    @Test
    public void testMockCreation(){
        assertNotNull(flightTimingDTO);
    }

}