package com.searchflights.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
  * This class convert string date into its well format.
  * @author akash
  *
  */
public class DateUtil {

    private DateUtil(){}
    public static LocalDateTime convertDate(String date) {
        if (!date.contains("-")) {
            date = date.replaceAll("\\/", "-");
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("M-dd-yyyy H:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(date.trim(), format);
        return dateTime;
    }
}