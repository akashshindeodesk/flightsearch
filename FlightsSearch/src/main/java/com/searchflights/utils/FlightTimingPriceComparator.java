package com.searchflights.utils;

import com.searchflights.dto.FlightTimingDTO;

import java.util.Comparator;

/**
  * This comparator compares two FLightTimingDTO  by their price.
  * @author akash
  *
  */
public class FlightTimingPriceComparator implements Comparator<FlightTimingDTO> {
    @Override
    public int compare(FlightTimingDTO flightTimingObj1, FlightTimingDTO flightTimingObj2) {
        return ((Integer)Math.round(flightTimingObj1.getActualPrice())) - ((Integer) Math.round(flightTimingObj2.getActualPrice()));
    }
}