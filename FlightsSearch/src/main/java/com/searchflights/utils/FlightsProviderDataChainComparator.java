package com.searchflights.utils;

import com.searchflights.dto.FlightTimingDTO;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 *
 * This is a chained comparator that is used to sort a list by multiple
 * attributes by chaining a sequence of comparators of individual fields
 * together.
 * @author akash
 *
 */
public class FlightsProviderDataChainComparator implements Comparator<FlightTimingDTO> {

    private List<Comparator<FlightTimingDTO>> listComparators;

    @SafeVarargs
    public FlightsProviderDataChainComparator(Comparator<FlightTimingDTO>... comparators) {
        this.listComparators = Arrays.asList(comparators);
    }

    @Override
    public int compare(FlightTimingDTO flightObj1, FlightTimingDTO flightObj2) {
        for (Comparator<FlightTimingDTO> comparator : listComparators) {
            int result = comparator.compare(flightObj1, flightObj2);
            if (result != 0) {
                return result;
            }
        }
        return 0;
    }
}