package com.searchflights.utils;

import com.searchflights.dto.FlightTimingDTO;

import java.util.Comparator;

/**
  * This comparator compares two FLightTimingDTO  by their price.
  * @author akash
  *
  */
public class FlightDepartureTimeComparator implements Comparator<FlightTimingDTO> {
    @Override
    public int compare(FlightTimingDTO flightTimingObj1, FlightTimingDTO flightTimingObj2) {
        return (flightTimingObj1.getDePartureTime()).compareTo(flightTimingObj2.getDePartureTime());
    }
}