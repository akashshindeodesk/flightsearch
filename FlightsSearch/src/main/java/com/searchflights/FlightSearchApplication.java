package com.searchflights;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author akash
 * <p>
 *   Main application class used to start the application.
 * </p>
 */

@SpringBootApplication
@EnableWebMvc
public class FlightSearchApplication {
    public static void main(String[] args) {
        SpringApplication.run(FlightSearchApplication.class, args);
    }

}