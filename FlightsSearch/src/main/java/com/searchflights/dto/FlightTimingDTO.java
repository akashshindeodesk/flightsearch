package com.searchflights.dto;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Objects;

/**
  * This class act DTO used to sort the flight provider data.
  * @author akash
  *
  */
public class FlightTimingDTO implements Comparable<FlightTimingDTO> {

    private String oRigin;
    private LocalDateTime dePartureTime;
    private String destination;
    private LocalDateTime destinationTime;
    private String price;
    private Float actualPrice;
    private String actualDepartureTime;
    private String actualDestinationTime;

    public String getoRigin() {
        return oRigin;
    }

    public LocalDateTime getDePartureTime() {
        return dePartureTime;
    }

    public String getDestination() {
        return destination;
    }

    public LocalDateTime getDestinationTime() {
        return destinationTime;
    }

    public String getPrice() {
        return price;
    }

    public void setoRigin(String oRigin) {
        this.oRigin = oRigin;
    }

    public void setDePartureTime(LocalDateTime dePartureTime) {
        this.dePartureTime = dePartureTime;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setDestinationTime(LocalDateTime destinationTime) {
        this.destinationTime = destinationTime;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Float getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Float actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getActualDepartureTime() {
        return actualDepartureTime;
    }

    public void setActualDepartureTime(String actualDepartureTime) {
        this.actualDepartureTime = actualDepartureTime;
    }

    public String getActualDestinationTime() {
        return actualDestinationTime;
    }

    public void setActualDestinationTime(String actualDestinationTime) {
        this.actualDestinationTime = actualDestinationTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightTimingDTO that = (FlightTimingDTO) o;
        return Objects.equals(oRigin, that.oRigin) &&
                Objects.equals(dePartureTime, that.dePartureTime) &&
                Objects.equals(destination, that.destination) &&
                Objects.equals(destinationTime, that.destinationTime) &&
                Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(oRigin, dePartureTime, destination, destinationTime, price);
    }

    @Override
    public String toString() {
        return "FlightTimingDTO{" +
                "oRigin='" + oRigin + '\'' +
                ", dePartureTime=" + dePartureTime +
                ", destination='" + destination + '\'' +
                ", destinationTime=" + destinationTime +
                ", price='" + price + '\'' +
                '}';
    }

    @Override
    public int compareTo(FlightTimingDTO flightTimingDTO) {
        return Comparator.comparing(FlightTimingDTO::getoRigin)
                .thenComparing(FlightTimingDTO::getDestination)
                .thenComparing(FlightTimingDTO::getDePartureTime)
                .thenComparing(FlightTimingDTO::getDestinationTime)
                .thenComparing(FlightTimingDTO::getActualPrice)
                .compare(this, flightTimingDTO);
    }
}
