package com.searchflights.controller;

import com.searchflights.config.ProvidersFilesConfig;
import com.searchflights.dto.FlightTimingDTO;
import com.searchflights.service.ISearchFlights;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;

/**
  * This class act SearchFlightsController used to provide REST Interface.
  * @author akash
  *
  */
@RestController
@RequestMapping("/")
@Api(value = "Search Flights Controller", description = "Operations pertaining to Search flights")
public class SearchFlightsController {
    private static final Logger LOG = LoggerFactory.getLogger(SearchFlightsController.class);

    @Autowired
    private ISearchFlights iSearchFlights;

    @Autowired
    private ProvidersFilesConfig providersFilesConfig;


    @GetMapping(value = "searchFlights/{Origin}/{Destination}" ,produces = "text/html")
    public ResponseEntity<String> searchFlights(@PathVariable("Origin")  @NotBlank @Validated String origin, @PathVariable("Destination")  @NotBlank @Validated String destination) {
        LOG.info("Get  Search flights request received {} , {} ", origin, destination);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(org.springframework.http.MediaType.TEXT_PLAIN);

        List<FlightTimingDTO> result = iSearchFlights.getSearchedFlights(origin,destination);

        String response  = providersFilesConfig.buildHTTPResponse(result,origin,destination);

        return new ResponseEntity(response, httpHeaders, HttpStatus.OK);
    }

    @ExceptionHandler(value = { ConstraintViolationException.class })
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleResourceNotFoundException(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        StringBuilder strBuilder = new StringBuilder();
        for (ConstraintViolation<?> violation : violations ) {
            strBuilder.append(violation.getMessage() + "\n");
        }
        return strBuilder.toString();
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }
}
