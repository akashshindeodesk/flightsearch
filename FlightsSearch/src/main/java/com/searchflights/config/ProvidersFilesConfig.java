package com.searchflights.config;

import com.searchflights.dto.FlightTimingDTO;
import com.searchflights.utils.DateUtil;
import com.searchflights.utils.FlightDepartureTimeComparator;
import com.searchflights.utils.FlightTimingPriceComparator;
import com.searchflights.utils.FlightsProviderDataChainComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * This class deals with providers and read the providers data and sort , remove duplicate.
 *
 * @author  akash
 *
 * ***/

@Configuration
public class ProvidersFilesConfig {
    private static final Logger LOG = LoggerFactory.getLogger(ProvidersFilesConfig.class);

    public List<FlightTimingDTO> processInputFile() {
        Set<FlightTimingDTO> inputList = new TreeSet<>();
        List<String> fileNamesList = new ArrayList<>();
        try {
            Files.newDirectoryStream(Paths.get("providers/"),
                    path -> path.toString().endsWith(".csv")).forEach(
                    filePath -> fileNamesList.add(filePath.toString()));

            for (String fileName : fileNamesList) {
                File file = ResourceUtils.getFile(fileName);
                try {
                    InputStream inputFS = new FileInputStream(file);
                    BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
                    // skip the header of the csv
                    inputList.addAll(br.lines().skip(1).map(mapToItem).collect(Collectors.toSet()));
                    br.close();
                } catch (IOException e) {
                    LOG.error("ERROR {}", e.getMessage());
                }
            }
        } catch (IOException e) {
            LOG.info("ERROR: {}", e.getMessage());
        }
        LOG.info("Input list size: {}", inputList.size());
        List<FlightTimingDTO> list = new ArrayList<>(inputList);
        Collections.sort(list, new FlightsProviderDataChainComparator(
                new FlightDepartureTimeComparator(),new FlightTimingPriceComparator()
        ));
        return list;
    }

    private Function<String, FlightTimingDTO> mapToItem = (line) -> {
        String[] p = null;

        if (line.contains("|")) {
            p = line.split("\\|");
        } else {
            p = line.split(",");
        }
        // a CSV has comma separated lines
        FlightTimingDTO item = new FlightTimingDTO();
        if (p.length > 4) {
            if (p[0] != null && p[0].trim().length() > 0) {
                item.setoRigin(p[0]);
            }
            if (p[1] != null && p[1].trim().length() > 0) {
                item.setDePartureTime(DateUtil.convertDate(p[1]));
            }
            if (p[1] != null && p[1].trim().length() > 0) {
                item.setActualDepartureTime(p[1]);
            }
            if (p[2] != null && p[2].trim().length() > 0) {
                item.setDestination(p[2]);
            }
            if (p[3] != null && p[3].trim().length() > 0) {
                item.setDestinationTime(DateUtil.convertDate(p[3]));
            }
            if (p[3] != null && p[3].trim().length() > 0) {
                item.setActualDestinationTime(p[3]);
            }
            if (p[4] != null && p[4].trim().length() > 0) {
                item.setPrice(p[4]);
            }
            if (p[4] != null && p[4].trim().length() > 0) {
                item.setActualPrice(Float.parseFloat(p[4].replace("$", "")));
            }
            return  item;
        } else {
            LOG.info("Remove empty lines from provider file {}", line);
        }
        return null;
    };


    /**
     * This method sends flights in ascending order to UI as per param received.
     *
     *
     * **/
    public List<FlightTimingDTO> getFlightResult(List<FlightTimingDTO> list,String origin, String destination) {

        List<FlightTimingDTO> result = list.stream().filter(item -> item.getoRigin().equals(origin) && item.getDestination().equals(destination))
                .collect(Collectors.toList());

        LOG.info("SIZE after filtering {}  ", result.size());
        return result;
    }

    /**
     * This method build the response that we are sending in response in TEXT /PLAIN format.
     *
     *
     * **/
    public  String buildHTTPResponse(List<FlightTimingDTO> result,String origin, String destination) {
        String response = "";
        if (!result.isEmpty()) {
            for (FlightTimingDTO flightTimingDTO : result) {
               response = response.concat(flightTimingDTO.getoRigin() + " --> " + flightTimingDTO.getDestination() + "  (" + flightTimingDTO.getActualDepartureTime() + " --> " +
                        flightTimingDTO.getActualDestinationTime() + ")" + " - " + flightTimingDTO.getPrice() +"\n");
            }
        } else {
            response = response.concat("No Flights Found for " + origin + " --> " + destination);
        }
        LOG.info("Final response {} ", response);
        return response;
    }
}