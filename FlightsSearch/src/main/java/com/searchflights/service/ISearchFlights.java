package com.searchflights.service;

import com.searchflights.dto.FlightTimingDTO;

import java.util.List;

public interface ISearchFlights {
    List<FlightTimingDTO> getSearchedFlights(String origin, String destination);
}
