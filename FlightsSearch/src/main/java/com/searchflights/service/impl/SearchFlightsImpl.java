package com.searchflights.service.impl;

import com.searchflights.config.ProvidersFilesConfig;
import com.searchflights.dto.FlightTimingDTO;
import com.searchflights.service.ISearchFlights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ISearchFlights")
public class SearchFlightsImpl implements ISearchFlights {
   @Autowired
   private ProvidersFilesConfig providersFilesConfig;

    @Override
    public List<FlightTimingDTO> getSearchedFlights(String origin, String destination) {

        List <FlightTimingDTO> sortedList = providersFilesConfig.processInputFile();

        return  providersFilesConfig.getFlightResult(sortedList,origin,destination);
    }
}
