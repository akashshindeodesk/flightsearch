I have developed Spring boot web application:
It has following features :

1] Swagger documentation 2.0 
2] HTTPS supports but I commented out code as its not part of assignment
3] Java 8 features including lambra  expression and Date time APIS.
4] Mockito Test cases 
5] Collections APIS of java 8 for sorting the providers data.
5] Validation of spring framework.
6] Rest Interface as said in assignment
7] Removed duplicate entries from the providers.
8] Order by two fields: by “Price” first in ascending order (lowest price first) and by “Departure Time” next (earlier flight first). 
9] No found flights will display message. 

To access Swagger documentation use URL
http://localhost:9000/swagger-ui.html#/



Request And Response:

1.
http://localhost:9000/searchFlights/LAS/AKS

No Flights Found for LAS --> AKS

2.
http://localhost:9000/searchFlights/LAS/LAX

LAS --> LAX  (6/23/2014 13:30:00 --> 6/23/2014 14:40:00) - $151.00
LAS --> LAX  (6/29/2014 14:55:00 --> 6/29/2014 16:10:00) - $201.00

3.
http://localhost:9000/searchFlights/YYC/YVR

YYC --> YVR  (6/12/2014 11:00:00 --> 6/12/2014 11:24:00) - $379.00


4.
http://localhost:9000/searchFlights/YVR/YYC/

No Flights Found for YVR --> YYC




How to Run:

Pre Requirement:
Java 1.8.0+
Maven 3.0.0 +

To Install maven : Use below link

For windows:
https://www.mkyong.com/maven/how-to-install-maven-in-windows/

For Linux: 
https://www.mkyong.com/maven/how-to-install-maven-in-ubuntu/

STEPS:

Step 1:
Extract the ZIP folder.
ex : i extracted into '/home/akash/demo' folder

Step 2: 
Goto the project folder.
Open terminal on the path where the project is 
Ex - Linux:  home/akash/demo/FlightNetwork

Step 3:
Fire command 
mvn clean install

After you see "Build Success" Message .
Step 4:
Copy 'flightnetwork-web.jar' into 'FlightNetwork' folder 
 Ex : cp target/FlightsSearch-web.jar .
 
Step 5:
Run command 

java -jar FlightsSearch-web.jar


Open the browser and hit :

http://localhost:9000/LAS/LAX

try with different combination. 


Please Note : 
Keep the providers folder parallel to where spring boot jar is .











































































































































































































































































































 

